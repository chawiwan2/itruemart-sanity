@footer_feature
Feature: Footer links and display
    As an online-shopper,
    I want to see information and navigate to another page from footer.
    With this feature I can ...

@TC_Footer2-1
Scenario: Can replace window Special Offers&Promotions in footer.
    Given User has [HeaderFooter] page opened
     When User clicks the [everyday-wow menuFT]
     Then The page url contains 'itruemart.com/everyday-wow'

@TC_Footer2-2
Scenario: Can replace window Special Offers&Promotions of Discount.
    Given User has [HeaderFooter] page opened
     When User clicks the [Special-Discount menuFT]
     Then The page url contains 'itruemart.com/discount-products'

@TC_Footer3-1
Scenario: Can replace window Our service-profile.
    Given User has [HeaderFooter] page opened
     When User clicks the [Our service-profile menu]
     Then The page url contains 'itruemart.com/member/profile'

@TC_Footer3-2
Scenario: Can replace window Our service-product in cart.
    Given User has [HeaderFooter] page opened
     When User clicks the [product in cart menu]
     ##login > if have't prodct in cart -> Then The page url contain 'itruemart.com/checkout/no-item'
     ##login > if have prodct in cart -> http://www.itruemart.com/checkout/step2

@TC_Footer3-3
Scenario: Can replace window Our service-my cart.
    Given User has [HeaderFooter] page opened
     When User clicks the [my cart menu]
     ##login > if have't prodct in cart -> Then The page url contain 'itruemart.com/checkout/no-item'
     ##login > if have prodct in cart -> http://www.itruemart.com/checkout/step2

## need login before
@TC_Footer3-4 @wip
Scenario: Can replace window Our service-checking my order.
    Given User has [HeaderFooter] page opened
     When User clicks the [checking my order menu]
     Then The page url contains 'itruemart.com/member/orders'

@TC_Footer3-5
Scenario: Can replace window our service-return policy.
    Given User has [HeaderFooter] page opened
     When User clicks the [return policy menu]
     Then The page url contains 'itruemart.com/policy/returnpolicy'

@TC_Footer3-6
Scenario: can replace window our service-Delivery policy.
    Given User has [HeaderFooter] page opened
     When User clicks the [delivery policy menu]
     Then The page url contains 'itruemart.com/policy/freedelivery'

@TC_Footer3-7
Scenario: Can replace window our service-refund policy.
    Given User has [HeaderFooter] page opened
     When User clicks the [refund policy]
     Then The page url contains 'itruemart.com/policy/moneyback'
@TC_Footer3-8
Scenario: Can replace window our service-guide to pay.
    Given User has [HeaderFooter] page opened
     When User clicks the [guide to pay menu]
     Then The page url contains 'itruemart.com/payment-manual'

@TC_Footer3-9
Scenario: Can replace window our service-Manual vouchers.
    Given User has [HeaderFooter] page opened
     When User clicks the [Manual vouchers menu]
     Then The page url contains 'itruemart.com/checkout/manual#fill'

@TC_Footer4-1
##confirm check xpath ccw tab
Scenario: Can Replce Window payment-ccw channel -> click on text.
    Given User has [HeaderFooter] page opened
    Then The [payment-ccw channel] is visible
     When User clicks the [payment-ccw channel]
     Then The page url contains 'itruemart.com/payment-manual'
     ##Then The [Tab payment-ccw channel] is visibles

@TC_Footer4-1-1
##confirm check xpath ccw tab
Scenario: Can Replce Window payment-ccw channel -> click on icon ccw.
    Given User has [HeaderFooter] page opened
    Then The [icon-ccw] is visible
     When User clicks the [icon-ccw]
     Then The page url contains 'itruemart.com/payment-manual'
     ##Then The [Tab payment-ccw channel] is visibles

@TC_Footer4-2
Scenario: Can replace Window paymant-payment couter channel on text payment couter.
    Given User has [HeaderFooter] page opened
    Then The [payment-payment counter channel] is visible
     When User clicks the [payment-payment counter channel]
     Then The page url contains 'itruemart.com/payment-manual#payment-counter'
     ##Then The [Tab payment-couter service channel] is visible

@TC_Footer4-2-1
Scenario: Can replace Window paymant-payment couter channel on icon payment couter.
    Given User has [HeaderFooter] page opened
    Then The [icon-payment counter] is visible
     When User clicks the [icon-payment counter]
     Then The page url contains 'itruemart.com/payment-manual#payment-counter'
     ##Then The [Tab payment-couter service channel] is visible

@TC_Footer4-3
Scenario: Can replace window payment-bank ATM channel on text bank ATM.
    Given User has [HeaderFooter] page opened
     Then The [payment-bank ATM channel] is visible
     ##click icon ATM to itruemart.com/payment-manual#atm
     When User clicks the [payment-bank ATM channel]
     Then The page url contains 'itruemart.com/payment-manual#atm'
     ##Then The [Tab payment-bank ATM channel] is visible

@TC_Footer4-3-1
Scenario: Can replace window payment-bank ATM channel on icon bank ATM.
    Given User has [HeaderFooter] page opened
     Then The [icon-ATM] is visible
     ##click icon ATM to itruemart.com/payment-manual#atm
     When User clicks the [icon-ATM]
     Then The page url contains 'itruemart.com/payment-manual#atm'
     ##Then The [Tab payment-bank ATM channel] is visible

@TC_Footer4-4
Scenario: Can replace window payment-Collect on dilivery channel.
    Given User has [HeaderFooter] page opened
     Then The [icon-COD] is visible
     And The [payment-COD] is visible
     ##no link go to COD

@TC_Footer4-5
Scenario: Can replace window payment-Payment Counter channel on text Payment Counter.
    Given User has [HeaderFooter] page opened
     Then The [payment-Counter service channel] is visible
     When User clicks the [payment-Counter service channel]
     Then The page url contains 'itruemart.com/payment-manual#counter-service'
     ##Then The [Tab payment-Payment Counter channel] is visible

@TC_Footer4-5-1
Scenario: Can replace window payment-Payment Counter channel on icon Payment Counter.
    Given User has [HeaderFooter] page opened
     Then The [icon-Counter service] is visible
     When User clicks the [icon-Counter service]
     Then The page url contains 'itruemart.com/payment-manual#counter-service'
     ##Then The [Tab payment-Payment Counter channel] is visible

@TC_Footer4-6
Scenario: Can replace window payment-internet Banking channel on text internet Banking.
    Given User has [HeaderFooter] page opened
    Then The [payment-internet Banking channel] is visible
     When User clicks the [payment-internet Banking channel]
     Then The page url contains 'itruemart.com/payment-manual#ibanking'
     ##Then The [Tab payment-internet Banking channel] is visible

@TC_Footer4-6
Scenario: Can replace window payment-internet Banking channel on icon internet Banking.
    Given User has [HeaderFooter] page opened
    Then The [icon-iBanking] is visible
     When User clicks the [icon-iBanking]
     Then The page url contains 'itruemart.com/payment-manual#ibanking'
     ##Then The [Tab payment-internet Banking channel] is visible

@TC_Footer5
Scenario: Can check box i'm itruemart.
    Given User has [HeaderFooter] page opened
     Then The [text box] is visible

@TC_Footer6
Scenario: Can see facebook fanpage.
    Given User has [HeaderFooter] page opened
     Then The [facebook fanpage] is visible

@TC_Footer7-1
Scenario: Can go to Follow us facebook fanpage.
    Given User has [HeaderFooter] page opened
     When User clicks the [facebook fanpage]
     Then The page url contains 'facebook.com/itruemart'

@TC_Footer7-2
Scenario: Can go to Follow us Twitter fanpage.
    Given User has [HeaderFooter] page opened
     When User clicks the [Twitter fanpage]
     Then The page url contains 'twitter.com/itruemart'

@TC_Footer7-3
Scenario: Can go to Follow us google+ fanpage.
#ต้อง login ของ google ก่อน
    Given User has [HeaderFooter] page opened
     When User clicks the [google+ fanpage]
     Then The page url contains 'plus.google.com/117049201689047839595/posts'

@TC_Footer7-4
Scenario: Can go to Follow us Youtube.
    Given User has [HeaderFooter] page opened
     When User clicks the [Youtube]
     Then The page url contains 'youtube.com/user/itruemart'

@TC_Footer8-1
Scenario: can subscribe_news with email for appiled.
##ใช้เมล์ที่เคยสมัครแล้ว
    Given User has [HeaderFooter] page opened
     When User enters 'nooae_four@hotmail.com' to the [subscribe_news]
     And User clicks the [send email button]
     Then The [error-box] is visible
     ##คุณเคยลงทะเบียนรับข่าวสารจาก iTrueMart แล้ว

@TC_Footer8-2
Scenario: can't subscribe_news with not email.
    Given User has [HeaderFooter] page opened
     When User enters '0869896577' to the [subscribe_news]
      And User clicks the [send email button]
      Then The [error-box] is visible
      ##กรุณากรอกรูปแบบอีเมล์ให้ถูกต้อง

##@TC_Footer8-3
##Scenario: กรอกเมล์ที่ยังไม่เคยสมัคร >> คุณได้ทำการลงทะเบียนเพื่อรับข่าวสารจาก iTrueMart เรียบร้อยแล้ว

@TC_Footer9
Scenario: Can go to Follow us show coppy right.
    Given User has [HeaderFooter] page opened
     Then The [coppy right] is visible

@TC_Footer10
Scenario: Can check Banner CyberSource
    Given User has [HeaderFooter] page opened
     Then The [Banner CyberSource] is visible

##@TC_Footer11
##Scenario: Can show FB fanpage.
    ##Given User has [HeaderFooter] page opened
    ##Then The [iconFB] is visible
    ##Then The [likeFB button] is visible
    ##Then The [friendFB] is visible
## end footer
