@header_feature
Feature: Header links and display
    As an online-shopper,
    I want to navigate to another page from header.
    With this feature I can ...

@TC_Header2 @header
Scenario: Can display contact.
    Given User has [HeaderFooter] page opened
    Then The [contact-us] is visible
    ##display ติดต่อ 029009999 no link

@TC_Header3 @header
Scenario: Can change language TH wow page.
    Given User has [HeaderFooter] page opened
    Then The [language TH button] is visible
    And The page url contains 'itruemart.com'

@TC_Header4 @header
Scenario: Can change language EN wow page.
    Given User has [HeaderFooter] page opened
    When User selects the [dropdown language EN]
    Then User clicks the [language EN button]
    And the page url contains 'itruemart.com/en'

@TC_Header5 @header
Scenario: Can Go to register page.
    Given User has [HeaderFooter] page opened
     When User clicks the [register]
     Then The page url contains 'itruemart.com/users/register'

@TC_Header6 @header
Scenario: Can Go to Login page.
    Given User has [HeaderFooter] page opened
     When User clicks the [Login]
     Then The page url contains 'itruemart.com/auth/login'

@TC_Login
Scenario: Can Login.
    Given User has [HeaderFooter] page opened
    When User clicks the [Login]
    And User enters 'nooae_four@hotmail.com' to the [login_username]
    And User enters '555555' to the [login_password]
    And User clicks the [button_login]
    #login จากหน้าไหนเมื่อlogin แล้วจะอยู่หน้านั้น

@TC_Header7 @header
Scenario: Can click icon itruemart go to home itruemart.
    Given User has [HeaderFooter] page opened
     When User clicks the [icon itruemart]
     Then The page url contains 'itruemart.com'

@TC_Header8 @header
Scenario: Can check searh ok -> box search, cate search, seach button.
    Given User has [HeaderFooter] page opened
    Then The [boxseach] is visible
    And The [boxseach] value is empty
    And The [Category-search] is visible
    And The [search button] is visible

@TC_Header8-1 @header @wip
Scenario: Can click category on seach.
    Given User has [HeaderFooter] page opened
     When User clicks the [Category-search]
     Then The [Cat1AtTopBar Search] value is not empty

@TC_Header-Cart
Scenario: Can see amount cart on header.
    Given User has [HeaderFooter] page opened
     Then The [header-cart] is visible

#@TC_Header8
#Scenario: Can search product or brand.
    #Given User has [HeaderFooter] page opened
     #When User enters 'iphone' to the [text box search]
     #And User clicks the [search button]
     #Then The browser shows [header_footer_SearchResult] page
     #And The [product] is visible

#@TC_Header9-1
#Scenario: Show 5 Category-search at top bar menu
    #Given User has [HeaderFooter] page opened
     #When User clicks the [CatAtTopBar Search]
     #Then The [categoryAtTopBar Search] value is not empty

#@TC_Header9-2
#Scenario: Can insert text for sesrch in box search.

##@TC_Header9-2
##Scenario: Can click Category-search
    ##Given User has [everyday-wow] page opened
    ## When User selects the [Gadget search]
     ##And User clicks the [search button]
     ##หมวดหมู่สินค้า >> มือถือและแท็บเล็ต/แกตเจ็ต/เครื่องใช้ไฟฟ้า/อุปกรณ์มือถือและแท็บเล็ต
     ##สามารถค้นหาสินค้าแต่ละหมวดหมู่ได้

@TC_headercategory
Scenario: Can check cate top manu -> cate>subcate>brand>product(<=4).
    Given User has [HeaderFooter] page opened
     When User clicks the [CatAtTopBar]
     ##And User selects the [catetop2]
     And User moves mouse over the [catetop2]
     Then The [catetop2] value is not empty
     And The [subcate2] is visible
     And The [brandcate2] is visible
     And The [productcate2] is visible

##@TC_Header11
##Scenario: Show 5 categories under Category at top bar menu
    ##Given User has [everyday-wow] page opened
     ##When User clicks the [CatAtTopBar]
     ##Then The [ShopByBrands] is visible
     #And The [AllMobileTablet] is visible
     #And The [Gadget] is visible
     #And The [Accessories] is visible อุปกรณ์มือถือและแท็บเล็ต
     #And The [home-appliances] is visible เครื่องใช้ไฟฟ้าภายในบ้าน
     #And The [Electronic] is visible

#@TC_Header11-1
#Scenario: Go to list of brand page when clicking Shop By brand
    #Given User has [HeaderFooter] page opened
     #When User clicks the [Category menu1]
     #And User selects the [ShopByBrands]
     ##Then The page url is 'itruemart.com/shopbybrand'

#@TC_Header11-2
#Scenario: Go to list of brand page when clicking All Mobile & Tablet
    #Given User has [HeaderFooter] page opened
    #When User clicks the [Category menu2]
    #And User selects the [AllMobileTablet]
    ##Then The page url is 'www.alpha.itruemart.com/'

#@TC_Header11-3
#Scenario: Go to list of brand page when clicking Gadget
    #Given User has [HeaderFooter] page opened
    #When User clicks the [Category menu3]
    #And User selects the [Gadget]
    ##Then The page url is 'www.alpha.itruemart.com/'

#@TC_Header11-4
#Scenario: Go to list of brand page when clicking Accessories
    #Given User has [HeaderFooter] page opened
    #When User clicks the [Category menu4]
    #And User selects the [Accessories]
    ##Then The page url is 'www.alpha.itruemart.com/'

#@TC_Header11-5
#Scenario: Go to list of brand page when clicking Electronic&Fitness
    #Given User has [HeaderFooter] page opened
    #When User clicks the [Category menu5]
    #And User selects the [Electronic&Fitness]
    ##Then The page url is 'www.alpha.itruemart.com/'

#@TC_Header11-6
#Scenario: Can verify category top up show subcate&brand&product.
    #Given User has [HeaderFooter] page opened
     #When User clicks the [CatAtTopBar]
     #And User moves mouse over the [Category menu2]
     #Then The [CateMenu2-subcate] is visible
     #And The [name-CateMenu2-subcate] value is 'AllMobileTablet'
     #And The [CateMenu2-subcate1] is visible
     #And The [CateMenu2-subcate-brand1] is visible
     #And The [CateMenu2-product1] is visible

#@TC_Header11-7
#Scenario: Can click Subcategory under Category at top bar menu.
    #Given User has [HeaderFooter] page opened
     #When User clicks the [CatAtTopBar]
     #And User moves mouse over the [Category menu3]
     #And User clicks the [cateMenu3-subcate1]
     #Then The browser shows [URL-cate3-subcate1_SearchResult] page
     #And The [cate3-subcate1] is visible

#@TC_Header11-8
#Scenario: Can click brand under Category at top bar menu.
    #Given User has [HeaderFooter] page opened
     #When User clicks the [CatAtTopBar]
     #And User moves mouse over the [Category menu4]
     #And User clicks the [cate4-brand1]
     #Then The browser shows [URL-cate4-brand1_SearchResult] page
     #And The [name-cate4-brand1] is visible

#@TC_Header11-9
#Scenario: Can click product under Category at top bar menu.
    #Given User has [HeaderFooter] page opened
     #When User clicks the [CatAtTopBar]
     #And User moves mouse over the [Category menu4]
     #And User clicks the [cate4-product1]
     #Then The browser shows [URL-cate4-product1_SearchResult] page
     #And The [name-cate4-product1] is visible

@TC_headerEveryDay-wow
Scenario: Can go to everyday-wow page from home.
    Given User has [HeaderFooter] page opened
     When User clicks the [header-Wow]
      Then The page url contains 'itruemart.com/everyday-wow'

@TC_headerDiscount
Scenario: Can go to discount page from home.
    Given User has [HeaderFooter] page opened
     When User clicks the [header-Discount]
      Then The page url contains 'itruemart.com/discount-products'

@TC_headerApple
Scenario: Can go to Apple page frome home.
    Given User has [HeaderFooter] page opened
     When User clicks the [header-apple]
      Then The page url contains 'itruemart.com/search?collection=0&q=Apple'

@TC_headerSumsung
Scenario: Can go to Sumsung page frome home.
    Given User has [HeaderFooter] page opened
     When User clicks the [header-Sumsung]
     Then The page url contains 'itruemart.com/search?collection=0&q=Samsung'

@TC_header-bannerWoW
Scenario: Can go to every day Wow from Wow.
    Given User has [HeaderFooter] page opened
     When User clicks the [header-bannerWow]
      Then The page url contains 'itruemart.com/everyday-wow'
