@login_feature
Feature: User login

@TC_Login
Scenario: Can login with email
    Given User has [HeaderFooter] page opened
    When User clicks the [Login]
    And User enters 'nooae_four@hotmail.com' to the [login_username]
    And User enters '555555' to the [login_password]
    And User clicks the [button_login]
    Then the browser shows [HeaderFooter] page
    And the [user_menu] value contains 'nooae'
    When user clicks the [user_menu]
    And user clicks the [logout_menu]
    Then the browser shows [HeaderFooter] page
    #login จากหน้าไหนเมื่อlogin แล้วจะอยู่หน้านั้น